<?php

namespace App\Model;

use Core\App;
use Core\Kernel\AbstractModel;

class CreneauModel extends AbstractModel
{
    protected $id;
    protected $id_salle;
    protected $start_at;
    protected $nbrhours;
    protected static $table ='creneau';

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getIdSalle()
    {
        return $this->id_salle;
    }

    /**
     * @return mixed
     */
    public function getStartAt()
    {
        return $this->start_at;
    }

    /**
     * @return mixed
     */
    public function getNbrhours()
    {
        return $this->nbrhours;
    }

    public static function insert($post){
        App::getDatabase()->prepareInsert(
            "INSERT INTO ".self::$table."(id_salle,start_at,nbrehours) VALUES (?,?,?)",
            [$post['salle'],$post['horaire'],$post['heure']]
        );
    }
    public static function insertCreneau($post){
        App::getDatabase()->prepareInsert(
            "INSERT INTO creneau_user (id_creneau,id_user,created_at) VALUES (?,?,NOW())",
            [$post['creneau'],$post['user']]
        );
    }

    public static function getCreneau()
    {
        return App::getDatabase()->prepare("
                    SELECT s.id, s.title,c.id,c.start_at, c.nbrehours FROM " . self::getTable(). " AS c
                    LEFT JOIN salle AS s ON s.id = c.id_salle",
            [],get_called_class());
    }

    public static function getCreneauById($id)
    {
        return App::getDatabase()->prepare("
                    SELECT s.id, s.title,c.id,c.start_at, c.nbrehours FROM " . self::getTable(). " AS c
                    LEFT JOIN salle AS s ON s.id = c.id_salle
                    WHERE c.id=$id",

            [],get_called_class());
    }

}