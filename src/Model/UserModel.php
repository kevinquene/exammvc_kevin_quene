<?php

namespace App\Model;

use Core\App;
use Core\Kernel\AbstractModel;

class UserModel extends AbstractModel
{
    protected $id;
    protected $nom;
    protected $email;

    protected static $table ='user';

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    public static function insert($post){
        App::getDatabase()->prepareInsert(
            "INSERT INTO ".self::$table."(nom,email) VALUES (?,?)",
            [$post['nom'],$post['email']]
        );
    }
    public static function getUserByCreneau($idcreneau)
    {
        return App::getDatabase()->prepare("
                    SELECT u.id, u.nom,u.email,cu.created_at FROM creneau_user AS cu
                    LEFT JOIN ".self::$table." AS u ON u.id = cu.id_user
                    WHERE cu.id=$idcreneau",

            [],get_called_class());
    }
}