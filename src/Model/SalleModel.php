<?php

namespace App\Model;

use Core\App;
use Core\Kernel\AbstractModel;

class SalleModel extends AbstractModel
{
protected $id;
protected $title;
protected $maxuser;

protected static $table ='salle';

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return mixed
     */
    public function getMaxuser()
    {
        return $this->maxuser;
    }

    public static function insert($post){
        App::getDatabase()->prepareInsert(
        "INSERT INTO ".self::$table."(title,maxuser) VALUES (?,?)",
        [$post['titre'],$post['limite']]
        );
    }
}