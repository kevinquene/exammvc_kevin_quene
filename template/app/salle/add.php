<h1><?= $titre; ?></h1>
<form action="" method="post" novalidate id="ajout">
    <?php
    echo $form->label('titre');
    echo $form->input('titre');
    echo $form->error('titre');
    echo $form->label('limite');
    echo $form->input('limite','number');
    echo $form->error('limite');
    echo $form->submit('ajout','Enregistrer');
    ?>
</form>