<?php

namespace App\Controller;

use App\Model\CreneauModel;
use App\Model\SalleModel;
use App\Model\UserModel;
use App\Service\Form;
use App\Service\Validation;

class CreneauController extends BaseController
{
    public function add()
    {
        $titre='Ajouter un creneau';
        $selectsalle=SalleModel::all();
        $errors=[];
        $v =new Validation();
        if(!empty($_POST['ajout'])){
            $post=$this->cleanXss($_POST);
            $errors=$this->validate($post);
            if($v->IsValid($errors)){
                CreneauModel::insert($post);
                $this->addFlash('success','creneau enregitré!');
            }
        }
        $form=new Form($errors);
        $this->render('app.creneau.add',array(
            'form'=>$form,
            'errors'=>$errors,
            'titre'=>$titre,
            'salle'=>$selectsalle,
        ));
    }

    public function single($id)
    {
        $errors=[];
        $form=new Form($errors);
        $alluser=UserModel::getUserByCreneau($id);
        $users=UserModel::all();
        $creneau=$this->getByIdOr404($id);
        $this->render('app.creneau.single',array(
            'creneau'=>$creneau[0],
            'users'=>$users,
            'form'=>$form,
            'idcreneau'=>$id,
            'alluser'=>$alluser,
        ));
    }

    public function addUser()
    {
        if(!empty($_POST['ajout'])){
                $this->dump($_POST);
                CreneauModel::insertCreneau($_POST);
                //$this->addFlash('success','user enregitré!');
        }
    }
    private function getByIdOr404($id)
    {
        $creneau= CreneauModel::getCreneauById($id);
        if(empty($creneau)){
            $this->Abort404();
        }else{
            return $creneau;
        }
    }
    private function validate($post)
    {
        $errors=[];
        $salle=$post['salle'];
        $salleexist=SalleModel::findById($salle);
        if(empty($salleexist)){
            $errors['salle']='fatal error';
        }
        if(is_integer($post['heure'])){
            $errors['heure']='veuillez renseigner un nombre entier uniquement';
        }elseif($post['heure']<0){
            $errors['heure']='veuillez renseigner un nombre entier positif uniquement';
        }
        return $errors;
    }

}