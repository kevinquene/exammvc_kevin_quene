<h1><?php echo $titre; ?></h1>
<form action="" method="post" id="ajout" novalidate>
    <?php
    echo $form->label('Assigner une salle');
    echo $form->selectEntity('salle',$salle,'title');
    echo $form->error('salle');
    echo $form->label('horaire');
    echo $form->input('horaire','datetime-local');
    echo $form->error('horaire');
    echo $form->label('heure');
    echo $form->input('heure','number');
    echo $form->error('heure');
    echo $form->submit('ajout','Enregistrer');
    ?>
</form>