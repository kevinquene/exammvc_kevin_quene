<h1><?php echo "Détail du créneau du ".$creneau->getStartAt(); ?></h1>
<h2><?php echo "Dans la salle ".$creneau->title; ?></h2>
    <form action="<?php echo $view->path('add-user-creneau',array('id'=>$creneau->getId())) ?>" method="post" id="ajout" novalidate>
        <?php
        echo $form->label('Assigner un user');
        echo $form->selectEntity('user',$users,'email');
        echo $form->input('creneau','hidden',$idcreneau);
        echo $form->submit('ajout','Enregistrer');
        ?>
    </form>
<section id="listeUser" class="wrap table">
    <h2>Liste des users inscrits</h2>
    <table>
        <thead>
        <tr>
            <th>Email</th>
            <th>Nom</th>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach ($alluser as $u){?>
            <tr id="" class="ligneUser">
                <th><?php echo $u->getEmail();  ?></th>
                <th><?php echo $u->getNom();  ?></th>
            </tr>
        <?php }?>
        </tbody>
    </table>
</section>
