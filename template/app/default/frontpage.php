
<section id="listeUser" class="wrap table">
    <h2>Liste des users</h2>
    <table>
        <thead>
        <tr>
            <th>Email</th>
            <th>Nom</th>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach ($users as $u){?>
            <tr id="" class="ligneUser">
                <th><?php echo $u->getEmail();  ?></th>
                <th><?php echo $u->getNom();  ?></th>
            </tr>
        <?php }?>
        </tbody>
    </table>
</section>
<section id="listeSalle" class="wrap table">
    <h2>Liste des salles</h2>
    <table>
        <thead>
        <tr>
            <th>Nom de salle</th>
            <th>Nombre de place</th>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach ($salles as $s){?>
            <tr id="" class="ligneSalle" >
                <th><?php echo $s->getTitle();  ?></th>
                <th><?php echo $s->getMaxuser();  ?></th>
            </tr>
        <?php }?>
        </tbody>
    </table>
</section>
<section id="listeCreneaux" class="wrap table">
    <h2>Liste des créneaux</h2>
    <table>
        <thead>
        <tr>
            <th>Nom de salle</th>
            <th>Horaire</th>
            <th>Durée</th>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach ($creneaux as $c){?>
            <tr id="" class="ligneCreaneau" onclick="document.location.href='<?php echo $view->path('detail-creneau',array('id'=>$c->getId())); ?>'" >
                <th><?php echo $c->title;  ?></th>
                <th><?php echo $c->getStartAt();  ?></th>
                <th><?php echo $c->nbrehours." H";  ?></th>
            </tr>
        <?php }?>
        </tbody>
    </table>
</section>