<?php

namespace App\Controller;

use App\Model\SalleModel;
use App\Service\Form;
use App\Service\Validation;

class SalleController extends BaseController
{
    public function add()
    {
        $titre='Ajouter une salle';
        $errors=[];
        $v =new Validation();
        if(!empty($_POST['ajout'])){
            $post=$this->cleanXss($_POST);
            $errors=$this->validate($post,$v);
            if($v->IsValid($errors)){
                SalleModel::insert($post);
                $this->addFlash('success','Salle enregitré!');
            }
        }
        $form=new Form($errors);
        $this->render('app.salle.add',array(
            'form'=>$form,
            'errors'=>$errors,
            'titre'=>$titre
        ));
    }

    private function validate($post,$v)
    {
        $errors=[];
        $errors['titre']=$v->textValid($post['titre'],'titre',2,100);
        if(is_integer($post['limite'])===false){
            $errors['limite']='veuillez renseigner un nombre entier uniquement';
        }elseif($post['limite']<0){
            $errors['limite']='veuillez renseigner un nombre entier positif uniquement';
        }
        return $errors;

    }
}