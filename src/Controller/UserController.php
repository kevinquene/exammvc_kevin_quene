<?php

namespace App\Controller;

use App\Model\UserModel;
use App\Service\Form;
use App\Service\Validation;

class UserController extends BaseController
{
    public function add()
    {
        $titre='Ajouter un user';
        $errors=[];
        $v =new Validation();
        if(!empty($_POST['ajout'])){
            $post=$this->cleanXss($_POST);
            $errors=$this->validate($post,$v);
            if($v->IsValid($errors)){
                UserModel::insert($post);
                $this->addFlash('success','user enregitré!');
            }
        }
        $form=new Form($errors);
        $this->render('app.user.add',array(
            'form'=>$form,
            'errors'=>$errors,
            'titre'=>$titre
        ));
    }

    private function validate($post,$v)
    {
        $errors=[];
        $errors['nom']=$v->textValid($post['nom'],'nom',2,70);
        $errors['email']=$v->emailValid($post['email']);
        $emailexist =UserModel::findByColumn('email',$post['email']);
        if($emailexist==$post['email'])
        {
            $errors['email']='Adresse E-mail déjà utilisé!';
        }
        return $errors;
    }
}